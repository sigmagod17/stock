from dateutil.rrule import rrule, DAILY
from datetime import datetime
import time
from dateutil.parser import parse
import requests
import json
import os
import pymysql

now = datetime.now()
year = now.year
month = now.month

conn = pymysql.connect(host='localhost', user='roy', db='Stock', charset='utf8')
cur = conn.cursor()
cur.execute("select stock_id, stock_name, listed_date from stock_list  where stock_type = '上櫃'")
# cur.execute( "select stock_id, stock_name, listed_date from stock_list  where stock_type = '上市' and stock_id = '1723' ")

row = cur.fetchall()


def get_webmsg(chinese_date, stock_id_list, date):
    header = {
        'User-agent': 'Mozilla/5.0(Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36'}
    # print(stock_date)
    url_tpex = 'http://www.tpex.org.tw/web/stock/aftertrading/daily_close_quotes/stk_quote_result.php?l=zh-tw&o=json&d=' + chinese_date
    print(url_tpex)

    response = requests.get(url_tpex, headers=header)

    json_data = json.loads(response.text)

    for stock_data in json_data['aaData']:

        if stock_data[0] in stock_id_list:
            # try:
            insert_db(stock_data, date)
            # except:
            #     pass

    time.sleep(2)

def insert_db(stock_data, date):
    try:
        stock_data[1:17] = [item.replace(',', '') for item in stock_data[1:17]]
        if stock_data[3][:1] == '-':
            stock_data.append('-')
            stock_data[3] = stock_data[3][:1]
        elif stock_data[3] == '0.00':
            stock_data.append('0')
        elif stock_data[3] == '+':
            stock_data.append('+')
            stock_data[3] = stock_data[3][:1]
        else:
            stock_data.append('0')
        stock_data.append(stock_data[3][:1])
        print(stock_data)
        stock_data[3] = stock_data[3][1:]
        print(stock_data)
    # print(stock_data)
    #     print(stock_data[0], stock_data[1], date, stock_data[8], stock_data[9], stock_data[4], stock_data[5],
    #         stock_data[6], stock_data[2], stock_data[3], stock_data[8], stock_data[17])
    except:
        pass
    #1.股票代號 2.證券名稱 3.收盤價 4.漲跌 5.開盤價 6.最高價 7.最低價 8.均價 9.成交股數 10.成交金額(元) 11.成交筆數 12.最後委買價
    #13.最後委賣價 14.發行股數	 15.次日參考價 16.次日漲價 17.次日跌停價

    cur.execute(
        "INSERT INTO stock_record(stock_id, stock_name, trade_date, total_volume, strike_price, open_price, upper_price, lower_price, final_price, diff, trade_number, sign)"
        "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", (stock_data[0], stock_data[1], date,
                                                                    stock_data[8], stock_data[9], stock_data[4],
                                                                    stock_data[5], stock_data[6], stock_data[2],
                                                                    stock_data[3], stock_data[8], stock_data[17]))

    conn.commit()

    now = datetime.now()
    print(now, stock_data[0], stock_data[1], date)


# date_list = list(rrule(DAILY, dtstart=parse(str('2009/2/2')), until=parse(str(datetime.now().strftime('%Y/%m/%d')))))
date_list = list(rrule(DAILY, dtstart=parse(str('2018/8/3')), until=parse(str(datetime.now().strftime('%Y/%m/%d')))))

stock_id_list = []
for stock_id in row:
    stock_id_list.append(stock_id[0])

for date in date_list:

    chinese_date = date.strftime('%Y/%m/%d')
    chinese_date = chinese_date.replace(chinese_date[0:4], str(int(chinese_date[0:4]) - 1911))
    get_webmsg(chinese_date, stock_id_list, date)