# import requests
# from bs4 import BeautifulSoup
#
# url = 'https://goodinfo.tw/StockInfo/StockDetail.asp?STOCK_ID=6510'
#
# res = requests.get(url, headers={'User-agent': 'Mozilla/5.0(Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36'})
# soup = BeautifulSoup(res.content.decode("utf-8"), 'lxml')
# print(soup)
from dateutil.rrule import rrule, MONTHLY
from datetime import datetime
import time
from dateutil.parser import parse
import requests
import json
import os
import pymysql

now = datetime.now()
year = now.year
month = now.month

print(now, year, month)

conn = pymysql.connect (host = 'localhost', user = 'roy', db = 'Stock', charset='utf8')
cur = conn.cursor()
cur.execute( "select stock_id, stock_name, listed_date from stock_list  where stock_type = '上市' limit 3")
row = cur.fetchall()

def get_webmsg(stock_id, stock_name, stock_date):

    header = {'User-agent': 'Mozilla/5.0(Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36'}
    # print(stock_date)
    url_twse = 'http://www.twse.com.tw/exchangeReport/STOCK_DAY?response=json&date=' + stock_date + '&stockNo=' + stock_id
    print(url_twse)
    response = requests.get(url_twse, headers=header)
    json_data = json.loads(response.text)
    time.sleep(1)
    try:
        # for saledate, value1, value2, value3, value4, value5, value6, value7, value8 in json_data['data']:
        for stock_data in json_data['data']:
            stock_data[0] = stock_data[0].replace(stock_data[0][0:3], str(int(stock_data[0][0:3]) + 1911))
            # print(saledate)
            stock_data[1:9] = [item.replace(',', '') for item in stock_data[1:9]]

            # print(saledate, value1)
            # saledate = saledate.replace(saledate[0:3], str(int(saledate[0:3]) + 1911))
            # saledate = datetime.strptime(saledate, "%Y/%m/%d")
            # print(int(value1))
            stock_data.append(stock_data[7][:1])
            stock_data[7] = stock_data[7][1:]

            insert_db(stock_id, stock_name, stock_data)
    except:
        pass

def insert_db(stock_id, stock_name, stock_data):

    cur.execute("INSERT INTO stock_record(stock_id, stock_name, trade_date, total_volume, strike_price, open_price, upper_price, lower_price, final_price, diff, trade_number, sign)"
                "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", (stock_id, stock_name, stock_data[0],
                stock_data[1], stock_data[2],stock_data[3], stock_data[4], stock_data[5], stock_data[6], stock_data[7], stock_data[8], stock_data[9]))
    conn.commit()
    # print(json_data)
for sid , stock_name ,date in row:

    date_list = list(rrule(MONTHLY, dtstart=parse(str(date)), until=parse(str(now))))
    for stock_date in date_list:

        stock_date = stock_date.strftime('%Y%m%d')

        get_webmsg(sid, stock_name, stock_date)
    stock_year = date.year
    stock_month = date.month

    #
    # for year in range(stock_year, now.year + 1):
    #     for month
    #     print(stock_year)
    #     stock_year += 1

    # print(sid, stock_year, stock_month)

# header = {'User-agent': 'Mozilla/5.0(Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36'}
# url_twse = 'http://www.twse.com.tw/exchangeReport/STOCK_DAY?response=json&date=' + '20180101' + '&stockNo=' + '1101'
# response = requests.get(url_twse, headers = header)
# json_data = json.loads(response.text)
# # print(json_data['data'])
# for data in json_data['data']:
#     print(data)

# def get_webmsg(year, month, id):
#     date = str(year) + "{0:0=2d}".format(month) + '01'
#     sid = str(id)
#     url_twse = 'http://www.twse.com.tw/exchangeReport/STOCK_DAY?response=json&date=' + date + '&stockNo=' + sid
#     response = requests.get(url_twse)
#     json_data = json.loads(response.text)
#
#     print()
#     return smt
#
