#step 1 :read data and process null
import pandas as pd
import numpy as np


df=pd.read_csv('foxconn_2013-2017.csv')
# print(df.info())
# df.dropna(axis=1,how='any',inplace=True)
#print(df)
print(len(df))

# print(np.where(np.isnan(df)))
import matplotlib.pyplot as plt
df=df.apply(lambda s: pd.to_numeric(s,errors='ignore'))
# print(df.info())
df['close'].plot()
# print(df.iloc[0:2])
print(df)
# print(np.any(np.isnan(df)))

from sklearn import preprocessing

def Normalize(df):

    newdf = df.copy()
    scalerOpen = preprocessing.MinMaxScaler()
    newdf['open'] = scalerOpen.fit_transform(df['open'].values.reshape(-1, 1))
    scalerLow = preprocessing.MinMaxScaler()
    newdf['Low'] = scalerLow.fit_transform(df['low'].value.reshape(-1,1))
    scalerHigh = preprocessing.MinMaxScaler()
    newdf['High'] = scalerHigh.fit_transform(df['high'].value.reshape(-1,1))
    scalerVolume = preprocessing.MinMaxScaler()
    newdf['Volume'] = scalerVolume.fit_transform(df['volume'].value.reshape(-1,1))
    scalerClose = preprocessing.MinMaxScaler()
    newdf['Close'] = scalerClose.fit_transform(df['close'].value.reshape(-1,1))
    return newdf

df_norm = Normalize(df)

print(df_norm)

def Prepare(df):
    numFeatures = len(df.columns)
    dataValue = df.as_matrix()
    timeFrame = 20
    result = []
    for index in range(len(dataValue)-(timeFrame+1)):
        result.append(dataValue[index:index+(timeFrame+1)])
    result = np.array(result)
    num_train = round(0.9*result.shape[0])
    X_train = result[:int(num_train),:-1]
    Y_train = result[:int(num_train), :-1][:-1]
    X_test = result[:int(num_train), :-1]
    Y_test = result[:int(num_train), :-1][:-1]

    X_train=np.reshape(X_train,(X_train.shape[0],X_train.shape[1],numFeatures))
    X_test=np.reshape(X_test,(X_test.shape[0],X_test.shape[1],numFeatures))

    return X_train,X_test,Y_train,Y_test,numFeatures

X_train,X_test,Y_train,Y_test,numFeatures = Prepare(df_norm)